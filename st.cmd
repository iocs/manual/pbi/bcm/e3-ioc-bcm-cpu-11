require essioc
require adsis8300bcm

# set macros
epicsEnvSet("CONTROL_GROUP", "LAB-BCM11")
epicsEnvSet("AMC_NAME",      "Ctrl-AMC-005")
epicsEnvSet("AMC_DEVICE",    "/dev/sis8300-5")
epicsEnvSet("EVR_NAME",      "LAB-BCM11:Ctrl-EVR-101:")

epicsEnvSet("P",        "$(CONTROL_GROUP):")
epicsEnvSet("R",        "$(AMC_NAME):")
epicsEnvSet("PREFIX",   "$(P)$(R)")

# set BCM generic channel names
iocshLoad("bcm-channels.iocsh")

# set BCM specific channel names
epicsEnvSet("SYSTEM1_PREFIX", "LAB-011:PBI-BCM-001:")
epicsEnvSet("SYSTEM1_NAME",   "Lab BCM 11#1")

epicsEnvSet("SYSTEM2_PREFIX", "LAB-011:PBI-BCM-002:")
epicsEnvSet("SYSTEM2_NAME",   "Lab BCM 11#2")

epicsEnvSet("SYSTEM3_PREFIX", "LAB-011:PBI-BCM-003:")
epicsEnvSet("SYSTEM3_NAME",   "Lab BCM 11#3")

epicsEnvSet("SYSTEM4_PREFIX", "LAB-011:PBI-BCM-004:")
epicsEnvSet("SYSTEM4_NAME",   "Lab BCM 11#4")

epicsEnvSet("SYSTEM5_PREFIX", "LAB-011:PBI-BCM-005:")
epicsEnvSet("SYSTEM5_NAME",   "Lab BCM 11#5")

epicsEnvSet("SYSTEM6_PREFIX", "LAB-011:PBI-BCM-006:")
epicsEnvSet("SYSTEM6_NAME",   "Lab BCM 11#6")

epicsEnvSet("SYSTEM7_PREFIX", "LAB-011:PBI-BCM-007:")
epicsEnvSet("SYSTEM7_NAME",   "Lab BCM 11#7")

epicsEnvSet("SYSTEM8_PREFIX", "LAB-011:PBI-BCM-008:")
epicsEnvSet("SYSTEM8_NAME",   "Lab BCM 11#8")

epicsEnvSet("SYSTEM9_PREFIX", "LAB-011:PBI-BCM-009:")
epicsEnvSet("SYSTEM9_NAME",   "Lab BCM 11#9")

epicsEnvSet("SYSTEM10_PREFIX", "LAB-011:PBI-BCM-010:")
epicsEnvSet("SYSTEM10_NAME",   "Lab BCM 11#10")

# load BCM data acquistion
iocshLoad("$(adsis8300bcm_DIR)/bcm-main.iocsh", "CONTROL_GROUP=$(CONTROL_GROUP), AMC_NAME=$(AMC_NAME), AMC_DEVICE=$(AMC_DEVICE), EVR_NAME=$(EVR_NAME)")

# load common module
iocshLoad $(essioc_DIR)/common_config.iocsh

# custom autosave in order to have Lut ID PVs restored before the others
afterInit("makeAutosaveFileFromDbInfo('$(AS_TOP)/$(IOCDIR)/req/lutIDs.req','autosaveFieldsLutIDs')")
afterInit("create_monitor_set("lutIDs.req",5)")
afterInit("fdbrestore("$(AS_TOP)/$(IOCDIR)/save/lutIDs.sav")")
afterInit("epicsThreadSleep(2)")

afterInit("fdbrestore("$(AS_TOP)/$(IOCDIR)/save/settings.sav")")

# call iocInit
iocInit

date

